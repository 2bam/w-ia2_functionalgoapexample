﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;

public static class Ext {

    public static IEnumerable<Tuple<int, T>> Enumerate<T>(this IEnumerable<T> xs) {
		return Utility.InfiniteRange().Zip(xs);
	}

    public static bool In<T>(this T x, HashSet<T> set)
    {
        return set.Contains(x);
    }

    public static bool In<K, V>(this KeyValuePair<K, V> x, Dictionary<K, V> dict)
    {
		return dict.Contains(x);
    }

    public static void Update<K, V>(this Dictionary<K, V> a, Dictionary<K, V> b) {
		foreach(var kvp in b) {
			a[kvp.Key] = kvp.Value;
		}
	}

	public static HashSet<T> ToHashSet<T>(this IEnumerable<T> list) {
		return new HashSet<T>(list);
	}

    public static V DefaultGet<K, V>(
        this Dictionary<K, V> dict,
        K key,
        Func<V> defaultFactory
    )
    {
        V v;
        if (!dict.TryGetValue(key, out v))
            dict[key] = v = defaultFactory();
        return v;
    }

	//Takes until condition is met. Includes the item that matches.
	public static IEnumerable<T> TakeUntil<T>(
		this IEnumerable<T> list,
		Func<T, bool> predicate
	) {
		foreach(var x in list) {
			yield return x;
			if(predicate(x))
				yield break;
		}
	}
}
