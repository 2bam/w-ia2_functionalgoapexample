using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FList<T> : IEnumerable<T> {
	readonly IEnumerable<T> collection;
		
	public FList() {
		collection = new T[0];
	}

	public FList(T singleValue) {
		collection = new T[1] { singleValue };
	}
		
	public FList(IEnumerable<T> collection) {
		this.collection = collection;
	}

	//Specificity to resolve ambiguity between FList-IEnumerable
	public static FList<T> operator+(FList<T> lhs, FList<T> rhs) {
		return FList.New(lhs.collection.Concat(rhs.collection));
	}

	public static FList<T> operator+(FList<T> lhs, IEnumerable<T> rhs) {
		return FList.New(lhs.collection.Concat(rhs));
	}

	public static FList<T> operator+(IEnumerable<T> lhs, FList<T> rhs) {
		return FList.New(FList.New(lhs).collection.Concat(rhs));
	}

	//This isn't semantically correct, but it's cleaner to read.
	public static FList<T> operator+(FList<T> lhs, T rhs) {
		return lhs + FList.New(rhs);
	}

	//This isn't semantically correct, but it's cleaner to read.
	public static FList<T> operator+(T lhs, FList<T> rhs) {
		return FList.New(lhs) + rhs;
	}
		
	public IEnumerator<T> GetEnumerator() {
		foreach(var element in collection)
			yield return element;
	}
		
	IEnumerator IEnumerable.GetEnumerator() {
		return GetEnumerator();
	}
}

public static class FList {
	static public FList<T> ToFList<T>(this IEnumerable<T> lhs) {
		return New(lhs);
	}

	static public FList<T> New<T>() {
		return new FList<T>();
	}

	static public FList<T> New<T>(T singleValue) {
		return new FList<T>(singleValue);
	}
		
	static public FList<T> New<T>(IEnumerable<T> collection) {
		return new FList<T>(collection);
	}
}
