﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;

public static class Utility {
    public static IEnumerable<int> InfiniteRange(int start = 0, int step = 1) {
		int i = start;
		while(true) {
			yield return i;
			i += step;
		}
	}

	public static T Log<T>(T param, string message = null) {
		Debug.Log((message!=null ? message + ":" : "") +  param.ToString());
		return param;
	}

	public static IEnumerable<T> Generate<T>(T seed, Func<T, T> generator)
    {
        var accum = seed;
        while(true)
        {
            yield return accum;
            accum = generator(accum);
        }
    }
}
