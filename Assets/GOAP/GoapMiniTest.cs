﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class GOAPState
{
    public Dictionary<string, bool> values = new Dictionary<string, bool>();
    public GOAPAction generatingAction = null;
	public int step=0;

	public override bool Equals(object obj)
    {
		var other = obj as GOAPState;
		var result =
			other != null
			&& other.generatingAction == generatingAction	//Very important to keep or A*'s "previous" tree will overwrite paths.
			&& other.values.Count == values.Count
			&& other.values.All(kv => kv.In(values));
		return result;
    }

    public override int GetHashCode()
    {
		//Better hashing but slow.
		//var x = 31;
		//var hashCode = 0;
		//foreach(var kv in values) {
		//	hashCode += x*(kv.Key + ":" + kv.Value).GetHashCode);
		//	x*=31;
		//}
		//return hashCode;

		//Heuristic count+first value hash multiplied by polynomial primes
		return values.Count == 0 ? 0 : 31*values.Count + 31*31*values.First().GetHashCode();
    }

	public override string ToString() {
		var str = "";
		foreach(var kv in values.OrderBy(x => x.Key)) {
			str += (string.Format("{0:12} : {1}\n", kv.Key, kv.Value));
		}
        return ("--->" + (generatingAction != null ? generatingAction.name : "NULL") + "\n"+str);
	}
}

public class GOAPAction {
	public Dictionary<string, bool> preconditions { get; private set; }
	public Dictionary<string, bool> effects { get; private set; }
	public string name { get; private set; }
	public float cost { get; private set; }

	public GOAPAction(string name) {
		this.name = name;
		cost = 1f;
		preconditions = new Dictionary<string, bool>();
		effects = new Dictionary<string, bool>();
	}

	public GOAPAction Cost(float cost) {
		if(cost < 1f) {
			//Costs < 1f make the heuristic non-admissible. h() could overestimate and create sub-optimal results.
			//https://en.wikipedia.org/wiki/A*_search_algorithm#Properties
			Debug.Log(string.Format("Warning: Using cost < 1f for '{0}' could yield sub-optimal results", name));	
		}
		this.cost = cost;
		return this;
	}
	public GOAPAction Pre(string s, bool value) {
		preconditions[s] = value;
		return this;
	}
	public GOAPAction Effect(string s, bool value) {
		effects[s] = value;
		return this;
	}

}

public class GoapMiniTest : MonoBehaviour {

	//Returns null if no plan satisfies (or watchdog cuts it)
	public static IEnumerable<GOAPAction> GoapRun(GOAPState from, GOAPState to, IEnumerable<GOAPAction> actions) {
		const int WATCHDOG_MAX = 200;

        int watchdog = 0;
        var seq = AStar<GOAPState>.Run(
            from,
            to,
            (curr, goal) => goal.values.Count(kv => !kv.In(curr.values)),
            curr => to.values.All(kv => kv.In(curr.values)),
            curr =>
            {
                var l = new List<AStar<GOAPState>.Arc>();

				//Max steps protection
                if (watchdog == WATCHDOG_MAX)
                    return l;
                else
					watchdog++;

                foreach (var act in actions)
                {
                    // Filter by preconditions met
                    if (act.preconditions.All(kv => kv.In(curr.values)))
                    {
                        var st = new GOAPState();
                        st.generatingAction = act;
                        st.values.Update(curr.values);
                        st.values.Update(act.effects);
						st.step = curr.step+1;
                        l.Add(new AStar<GOAPState>.Arc(st, act.cost));
                    }
                }
                return l;
            }
        );

		//Can't plan.
		if(seq == null) return null;

        foreach (var act in seq.Skip(1))
        {
			Debug.Log(act);
        }
		Debug.Log("WATCHDOG " + watchdog);
		
		return seq.Skip(1).Select(x => x.generatingAction);
	}

    // Use this for initialization
    void Start() {
        var actions = new List<GOAPAction>() {
            new GOAPAction("BuildHouse")
                .Pre("hasWood", true)
                .Pre("hasHammer", true)
                .Effect("houseBuilt", true),
            new GOAPAction("CollectHammer")
                .Effect("hasHammer", true),
            new GOAPAction("CollectAxe")
                .Effect("hasAxe", true)
                .Cost(10f),
            new GOAPAction("CollectCheapAxe")
                .Effect("hasAxe", true)
                .Effect("backPain", true)
                .Cost(2f),
            new GOAPAction("ChopWood")
                .Pre("hasAxe", true)
                .Effect("hasWood", true),
            new GOAPAction("UseMedicine")
                .Effect("backPain", false)
                .Cost(100f)
        };

        var from = new GOAPState();
		from.values["backPain"] = false;

        var to = new GOAPState();
        to.values["houseBuilt"] = true;
        to.values["backPain"] = false;

		var plan = GoapRun(from, to, actions);

		if(plan == null)
			Debug.Log("Impossible to plan!");
		else {

			//foreach(var step in plan.Enumerate())
			//	Debug.Log((step.First+1) + ") " + step.Second.name);
			Debug.Log(
				plan
					.Enumerate()
					.Aggregate("Plan is:\n", (a, step) =>
						a += string.Format("{0}) {1}\n", step.First+1, step.Second.name)
					)
			);
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
